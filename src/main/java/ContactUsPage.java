import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ContactUsPage extends MainPage {
    {
        driver.get("http://taqc-dp170.zzz.com.ua/index.php?route=information/contact");
    }

        static WebDriver driver = new ChromeDriver();
        WebElement contactUsTitle;
        WebElement ourLocationInfoTitle;
        WebElement ourLocationInfoInformation;
        WebElement contactFormBlockTitle;
        WebElement yourNameInputForm;
        WebElement eMailAdressForm;
        WebElement enquryForm;
        WebElement submitButton;
        String expectedTitle;

     ContactUsPage(){
          contactUsTitle = driver.findElement(By.xpath("//*[@id=\"content\"]/h1"));
          ourLocationInfoTitle = driver.findElement(By.xpath("//*[@id=\"content\"]/h3"));
          ourLocationInfoInformation = driver.findElement(By.xpath("//*[@id=\"content\"]/div"));
          contactFormBlockTitle = driver.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset/legend"));
          yourNameInputForm = driver.findElement(By.xpath("//*[@id=\"input-name\"]"));
          eMailAdressForm = driver.findElement(By.xpath("//*[@id=\"input-email\"]"));
          enquryForm = driver.findElement(By.xpath("//*[@id=\"input-enquiry\"]"));
          submitButton = driver.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input"));
          expectedTitle = "Contact Us";
     }

    public void smokeWholePageVisibility(){

        ContactUsPage contactUsPage = new ContactUsPage();
        driver.manage().window().maximize();
        Assert.assertTrue(contactUsPage.contactUsTitle.isDisplayed());
        Assert.assertTrue(contactUsPage.ourLocationInfoTitle.isDisplayed());
        Assert.assertTrue(contactUsPage.ourLocationInfoInformation.isDisplayed());
        Assert.assertTrue(contactUsPage.contactFormBlockTitle.isDisplayed());
        Assert.assertTrue(contactUsPage.yourNameInputForm.isDisplayed());
        Assert.assertTrue(contactUsPage.eMailAdressForm.isDisplayed());
        Assert.assertTrue(contactUsPage.enquryForm.isDisplayed());
        Assert.assertTrue(contactUsPage.submitButton.isDisplayed());
        Assert.assertTrue(contactUsPage.submitButton.isEnabled());
        Assert.assertEquals(expectedTitle, driver.getTitle());
    }

    public void submittingFormInvalidData()  {


        ContactUsPage contactUsPage = new ContactUsPage();
        driver.manage().window().maximize();

        //1
        contactUsPage.submitButton.click();
        WebElement nameError = driver.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset/div[1]/div/div"));
        Assert.assertTrue(nameError.isDisplayed());
        Assert.assertEquals(nameError.getText(), ConstTextValues.nameErrorMsg);

        WebElement eMailError = driver.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset/div[2]/div/div"));
        Assert.assertTrue(eMailError.isDisplayed());
        Assert.assertEquals(eMailError.getText(), ConstTextValues.eMailErrorMsg);

        WebElement enquiryError = driver.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset/div[3]/div/div"));
        Assert.assertTrue(enquiryError.isDisplayed());
        Assert.assertEquals(enquiryError.getText(), ConstTextValues.enquiryErrorMsg);

        //2
        contactUsPage.yourNameInputForm.click();

        contactUsPage.yourNameInputForm.sendKeys(ConstTextValues.name);
        contactUsPage.submitButton.click();

        Assert.assertFalse(nameError.isDisplayed());

        Assert.assertTrue(eMailError.isDisplayed());
        Assert.assertEquals(eMailError.getText(), ConstTextValues.eMailErrorMsg);

        Assert.assertTrue(enquiryError.isDisplayed());
        Assert.assertEquals(enquiryError.getText(), ConstTextValues.enquiryErrorMsg);

        //3
        contactUsPage.eMailAdressForm.click();
        contactUsPage.eMailAdressForm.sendKeys(ConstTextValues.eMail);
        contactUsPage.submitButton.click();

        Assert.assertFalse(nameError.isDisplayed());
        Assert.assertFalse(eMailError.isDisplayed());

        Assert.assertTrue(enquiryError.isDisplayed());
        Assert.assertEquals(enquiryError.getText(), ConstTextValues.enquiryErrorMsg);

    }

    public void enquiryFormInvalidData(){


        ContactUsPage contactUsPage = new ContactUsPage();
        driver.manage().window().maximize();

        //1
        contactUsPage.yourNameInputForm.click();
        contactUsPage.yourNameInputForm.sendKeys(ConstTextValues.name);

        contactUsPage.eMailAdressForm.click();
        contactUsPage.eMailAdressForm.sendKeys(ConstTextValues.eMail);

        contactUsPage.enquryForm.click();
        contactUsPage.enquryForm.sendKeys(ConstTextValues.test4char);
        contactUsPage.submitButton.click();


        WebElement enquiryError = driver.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset/div[3]/div/div"));
        Assert.assertTrue(enquiryError.isDisplayed());
        Assert.assertEquals(enquiryError.getText(), ConstTextValues.enquiryErrorMsg);

        //2

        contactUsPage.enquryForm.clear();
        contactUsPage.enquryForm.sendKeys(ConstTextValues.chars3000Text);
        contactUsPage.submitButton.click();
        Assert.assertTrue(enquiryError.isDisplayed());
        Assert.assertEquals(enquiryError.getText(), ConstTextValues.enquiryErrorMsg);



    }





}
